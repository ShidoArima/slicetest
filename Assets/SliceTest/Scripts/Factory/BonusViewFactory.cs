﻿using SliceTest.Common;
using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Factory
{
	public class BonusViewFactory : MonoBehaviour, IFactory<UIBaseBonusView>
	{
		[SerializeField] UIBaseBonusView _bonusView;
		
		public UIBaseBonusView Create()
		{
			UIBaseBonusView bonusView = Instantiate(_bonusView);
			
			return bonusView;
		}
	}
}