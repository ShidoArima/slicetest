﻿using System;
using UnityEngine;

namespace SliceTest.UI
{
	public abstract class UIBaseBonusView : MonoBehaviour
	{
		public Vector3       WorldPosition  { get; protected set; }
		public bool          IsPlaying { get; protected set; }

		public event Action  OnComplete;
		public abstract void Init(Vector3 position, int bonus);

		public abstract void SetScreenPosition(Vector3 position);

		public virtual void Play()
		{
			IsPlaying = true;
		}

		public virtual void Stop()
		{
			IsPlaying = false;
		}

		protected virtual void Complete()
		{
			IsPlaying = false;
			OnComplete?.Invoke();
		}
	}
}