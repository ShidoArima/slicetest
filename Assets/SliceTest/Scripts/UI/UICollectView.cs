﻿using System;
using SliceTest.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SliceTest.UI
{
	public class UICollectView : MonoBehaviour
	{
		[SerializeField] TMP_Text _scoreText;
		[SerializeField] TMP_Text _localScoreText;
		[SerializeField] Button   _restartButton;
		IBonusController          _bonusController;

		public event Action OnRestartClick;

		public void Init(IBonusController bonusController)
		{
			_bonusController               =  bonusController;
			_bonusController.OnScoreChange += BonusController_OnScoreChange;
			
			if (_restartButton != null)
				_restartButton.onClick.AddListener(Restart_OnClick);
		}

		void OnDestroy()
		{
			_bonusController.OnScoreChange -= BonusController_OnScoreChange;
			_restartButton.onClick.RemoveListener(Restart_OnClick);
		}

		void BonusController_OnScoreChange(long score)
		{
			_scoreText.text = _bonusController.Score.ToString();
		}

		void Restart_OnClick()
		{
			OnRestartClick?.Invoke();
		}

		public void ShowCollection()
		{
			_scoreText.text      = _bonusController.Score.ToString();
			_localScoreText.text = _bonusController.Reward.ToString();
		}
	}
}