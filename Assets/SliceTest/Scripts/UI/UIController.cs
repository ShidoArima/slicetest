﻿using SliceTest.Controllers;
using SliceTest.UI.States;
using UnityEngine;

namespace SliceTest.UI
{
	public class UIController : MonoBehaviour
	{
		[SerializeField] UIGameplayView _gameplayView;
		[SerializeField] UICollectView  _collectView;
		[SerializeField] UIRespawnView  _respawnView;

		IState _currentState;

		public UIGameplayView GameplayView => _gameplayView;
		public UICollectView  CollectView  => _collectView;
		public UIRespawnView  RespawnView  => _respawnView;

		public void Init(IBonusController bonusController)
		{
			_gameplayView.Init(bonusController);
			_collectView.Init(bonusController);
			_respawnView.Init();
			
			_gameplayView.gameObject.SetActive(false);
			_collectView.gameObject.SetActive(false);
			_respawnView.gameObject.SetActive(false);
			
			IState state = new GameplayUIState(this);
			ChangeState(state);
		}

		public void Finish()
		{
			ChangeState(new CollectUIState(this));
		}

		public void Fall()
		{
			ChangeState(new RespawnUIState(this));
		}

		void Update()
		{
			IState newState = _currentState?.UpdateState();
			if(newState != null)
				ChangeState(newState);
		}

		void ChangeState(IState state)
		{
			while (true)
			{
				_currentState?.DestroyState();

				_currentState = state;

				if (_currentState != null)
				{
					_currentState.PrepareState();
					IState newState = _currentState.UpdateState();

					if (newState != null)
					{
						state = newState;
						continue;
					}
				}
				break;
			}
		}
	}
}