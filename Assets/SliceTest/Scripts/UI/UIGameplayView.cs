﻿using System;
using SliceTest.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SliceTest.UI
{
	public class UIGameplayView : MonoBehaviour
	{
		[SerializeField] Button   _menuButton;
		[SerializeField] TMP_Text _scoreText;
		IBonusController          _bonusController;
		
		public event Action OnMenuClick;
		
		public void Init(IBonusController bonusController)
		{
			_bonusController               =  bonusController;
			_bonusController.OnScoreChange += BonusController_OnScoreChange;
			
			if (_menuButton != null)
				_menuButton.onClick.AddListener(Menu_OnClick);
			
			UpdateScore(bonusController.Score);
		}
		
		void OnDestroy()
		{
			_bonusController.OnScoreChange -= BonusController_OnScoreChange;
			_menuButton.onClick.RemoveListener(Menu_OnClick);
		}

		void BonusController_OnScoreChange(long score)
		{
			_scoreText.text = _bonusController.Score.ToString();
		}

		void Menu_OnClick()
		{
			OnMenuClick?.Invoke();
		}
		
		void UpdateScore(long score)
		{
			if(_scoreText != null)
				_scoreText.text = score.ToString();
		}
	}
}