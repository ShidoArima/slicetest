﻿namespace SliceTest.UI.States
{
	public class CollectUIState : BaseUIState
	{
		readonly UICollectView _view;
		IState                 _nextState;
		
		public CollectUIState(UIController owner) : base(owner)
		{
			_view = owner.CollectView;
		}
		
		public override void PrepareState()
		{
			_view.gameObject.SetActive(true);
			_view.OnRestartClick += View_OnRestartClick;
			_view.ShowCollection();
		}
		
		public override IState UpdateState()
		{
			return _nextState;
		}

		public override void DestroyState()
		{
			_view.OnRestartClick -= View_OnRestartClick;
			_view.gameObject.SetActive(false);
		}
		
		void View_OnRestartClick()
		{
			_nextState = new RestartUIState(Owner);
		}
	}
}