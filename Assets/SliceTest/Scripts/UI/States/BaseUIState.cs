﻿namespace SliceTest.UI.States
{
	public abstract class BaseUIState : BaseMonoState<UIController>
	{
		protected BaseUIState(UIController owner) : base(owner)
		{ }
	}
}