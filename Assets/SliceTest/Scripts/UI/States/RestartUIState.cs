﻿using SliceTest.Managers;
using UnityEngine.SceneManagement;

namespace SliceTest.UI.States
{
	public class RestartUIState : BaseUIState
	{
		IState _nextState;
		
		public RestartUIState(UIController owner) : base(owner)
		{
		}
		
		public override void PrepareState()
		{
			int index =  SceneManager.GetActiveScene().buildIndex;
			LevelLoader.LoadLevel(index, ()=> _nextState = new GameplayUIState(Owner));
		}

		public override IState UpdateState()
		{
			return _nextState;
		}

		public override void DestroyState()
		{
		}
	}
}