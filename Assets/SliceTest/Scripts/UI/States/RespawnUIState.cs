﻿namespace SliceTest.UI.States
{
	public class RespawnUIState : BaseUIState
	{
		readonly UIRespawnView _view;
		IState                 _nextState;
		
		public RespawnUIState(UIController owner) : base(owner)
		{
			_view = owner.RespawnView;
		}
		
		public override void PrepareState()
		{
			_view.gameObject.SetActive(true);
			_view.OnRestartClick += View_OnRestartClick;
		}
		
		public override IState UpdateState()
		{
			return _nextState;
		}

		public override void DestroyState()
		{
			_view.OnRestartClick -= View_OnRestartClick;
			_view.gameObject.SetActive(false);
		}
		
		void View_OnRestartClick()
		{
			_nextState = new RestartUIState(Owner);
		}
	}
}