﻿namespace SliceTest.UI.States
{
	public class GameplayUIState : BaseUIState
	{
		readonly UIGameplayView _view;
		IState                  _nextState;
		
		public GameplayUIState(UIController owner) : base(owner)
		{
			_view = owner.GameplayView;
		}
		
		public override void PrepareState()
		{
			_view.gameObject.SetActive(true);
			_view.OnMenuClick += View_OnMenuClick;
		}
		
		public override IState UpdateState()
		{
			return _nextState;
		}

		public override void DestroyState()
		{
			_view.gameObject.SetActive(false);
			_view.OnMenuClick -= View_OnMenuClick;
		}
		
		void View_OnMenuClick()
		{
			_nextState = new RestartUIState(Owner);
		}
	}
}