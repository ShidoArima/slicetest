﻿using TMPro;
using UnityEngine;

namespace SliceTest.UI
{
	[RequireComponent(typeof(RectTransform))]
	public class UIBonusText : UIBaseBonusView
	{
		[SerializeField] Animation _animation;
		[SerializeField] TMP_Text  _text;

		RectTransform _rectTransform;
		bool          _playing;

		void Awake()
		{
			_rectTransform = GetComponent<RectTransform>();
		}

		public override void Init(Vector3 position, int bonus)
		{
			WorldPosition   = position;
			_text.text = $"+{bonus}";
		}

		public override void SetScreenPosition(Vector3 position)
		{
			_rectTransform.anchoredPosition = position;
		}

		public override void Play()
		{
			base.Play();
			
			_animation.Play();
		}

		public override void Stop()
		{
			base.Play();
			
			_animation.Stop();
		}

		void Update()
		{
			if(!IsPlaying || _animation.isPlaying)
				return;

			Complete();
		}
	}
}