﻿using System;
using SliceTest.Controllers;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SliceTest.UI
{
	public class UIRespawnView : MonoBehaviour
	{
		[SerializeField] Button   _restartButton;

		public event Action OnRestartClick;

		public void Init()
		{
			if (_restartButton != null)
				_restartButton.onClick.AddListener(Restart_OnClick);
		}

		void OnDestroy()
		{
			_restartButton.onClick.RemoveListener(Restart_OnClick);
		}

		void Restart_OnClick()
		{
			OnRestartClick?.Invoke();
		}
	}
}