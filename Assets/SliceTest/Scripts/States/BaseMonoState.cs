﻿using UnityEngine;

namespace SliceTest.UI.States
{
	public abstract class BaseMonoState<T> : IState where T : MonoBehaviour
	{
		protected T Owner { get; private set; }

		protected BaseMonoState(T owner)
		{
			Owner = owner;
		}

		public abstract void   PrepareState();
		public abstract IState UpdateState();
		public abstract void   DestroyState();
	}
}