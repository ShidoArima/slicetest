﻿using UnityEngine;

namespace SliceTest.UI.States
{
	public interface IState
	{
		void PrepareState();
		IState UpdateState();
		void DestroyState();
	}
}