﻿namespace SliceTest.Common
{
	public static class Layers
	{
		public static readonly int Blade        = 8;
		public static readonly int Handle       = 9;
		public static readonly int Floor        = 10;
		public static readonly int Ignore       = 11;
		public static readonly int BladeTrigger = 12;
		public static readonly int Obstacle     = 13;
		public static readonly int Respawn      = 14;
	}
}