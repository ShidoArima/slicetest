﻿using System.Collections.Generic;
using UnityEngine;

namespace SliceTest.Common
{
	public class ObjectPool<T> where T : MonoBehaviour
	{
		readonly IFactory<T> _factory;
		readonly Stack<T>    _freeObjects = new Stack<T>();

		public ObjectPool(IFactory<T> factory)
		{
			_factory = factory;
		}

		public T GetObject()
		{
			if (_freeObjects.Count > 0)
				return _freeObjects.Pop();

			return _factory.Create();
		}
		
		public void Release(T poolObject)
		{
			poolObject.gameObject.SetActive(false);
			_freeObjects.Push(poolObject);
		}

		public void Clear()
		{
			while (_freeObjects.Count > 0)
			{
				T poolObject = _freeObjects.Pop();
				Object.Destroy(poolObject.gameObject);
			}
		}
	}
}