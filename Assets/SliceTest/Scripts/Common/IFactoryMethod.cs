﻿namespace SliceTest.Common
{
	public interface IFactory<T>
	{
		T Create();
	}
}