﻿using System;
using UnityEngine;

namespace SliceTest.Controllers
{
    public class InputController : BaseInputController
    {
        bool _enabled;
        
        public override event Action OnClick;

        public override void EnableInput(bool enable)
        {
            _enabled = enable;
        }

        void Update()
        {
            if(!_enabled)
                return;
                
            if (Input.GetMouseButtonDown(0))
            {
                OnClick?.Invoke();
            }
        }
    }
}
