﻿using System;
using System.Collections.Generic;
using SliceTest.Common;
using SliceTest.Factory;
using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Controllers
{
	public class BonusController : MonoBehaviour, IBonusController
	{
		const string SCORE_PREF_KEY = "GlobalScoreKey";

		[SerializeField] BonusViewFactory _viewFactory;
		[SerializeField] RectTransform    _viewParent;

		Camera                      _gameCamera;
		ObjectPool<UIBaseBonusView> _pool;
		List<UIBaseBonusView>       _viewList;
		long                        _score;
		long                        _localScore;
		int                         _bonus;

		public event Action<long> OnScoreChange;

		public long Score  => _score;
		public long Reward => _localScore;

		void OnDestroy()
		{
			PlayerPrefs.SetString(SCORE_PREF_KEY, _score.ToString());
		}

		public void Init(Camera gameCamera)
		{
			_pool     = new ObjectPool<UIBaseBonusView>(_viewFactory);
			_viewList = new List<UIBaseBonusView>();

			long.TryParse(PlayerPrefs.GetString(SCORE_PREF_KEY, "0"), out _score);

			_gameCamera = gameCamera;
			_localScore = 0;
			_viewList.Clear();
		}

		public void Restart()
		{
			int count = _viewList.Count;
			for (int i = count - 1; i >= 0; i--)
			{
				UIBaseBonusView bonusView = _viewList[i];
				_viewList.RemoveAt(i);
				_pool.Release(bonusView);
			}

			_localScore = 0;
		}

		public void ShowBonus(Vector3 position, int bonus)
		{
			UpdateScore(bonus);

			UIBaseBonusView bonusView = _pool.GetObject();

			bonusView.transform.SetParent(_viewParent);
			bonusView.Init(position, bonus);
			bonusView.gameObject.SetActive(true);
			bonusView.Play();

			_viewList.Add(bonusView);
		}

		public void ShowBonusMultiplier(Vector3 position, int bonus)
		{
			ShowBonus(position, bonus);
			UpdateScore(_localScore * bonus);
		}

		void Update()
		{
			int count = _viewList.Count;
			for (int i = count - 1; i >= 0; i--)
			{
				UIBaseBonusView bonusView = _viewList[i];

				if (bonusView.IsPlaying)
					UpdateViewPosition(_gameCamera, _viewParent, bonusView);
				else
				{
					_viewList.RemoveAt(i);
					_pool.Release(bonusView);
				}
			}
		}

		void UpdateScore(long bonus)
		{
			if (bonus == 0)
				return;

			_score      += bonus;
			_localScore += bonus;
			OnScoreChange?.Invoke(_score);
		}

		static void UpdateViewPosition(Camera camera, RectTransform viewParent, UIBaseBonusView bonusView)
		{
			Vector2 screenPos = camera.WorldToScreenPoint(bonusView.WorldPosition);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(viewParent, screenPos, null, out Vector2 localPos);

			bonusView.SetScreenPosition(localPos);
		}
	}
}