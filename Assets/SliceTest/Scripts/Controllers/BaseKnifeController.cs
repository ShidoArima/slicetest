﻿using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Controllers
{
	public abstract class BaseKnifeController : MonoBehaviour
	{
		public abstract void Init(IInputController inputController, UIController uiController);
	}
}