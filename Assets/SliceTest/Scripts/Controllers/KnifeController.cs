﻿using System;
using SliceTest.Common;
using SliceTest.UI;
using SliceTest.Utils;
using UnityEngine;

namespace SliceTest.Controllers
{
	[RequireComponent(typeof(Rigidbody))]
	public class KnifeController : BaseKnifeController
	{
		#region constants

		const float FORWARD_DISTANCE = 0.2f;

		#endregion

		#region attributes

		[Header("Forces")] [SerializeField] Vector2        _upForce;
		[SerializeField]                    Vector2        _forwardForce;
		[SerializeField]                    Vector2        _backwardForce;
		[SerializeField]                    AnimationCurve _rotationCurve;

		[Header("Rotation")] [SerializeField] float _torque;
		[SerializeField]                      float _decelerationRate = 0.1f;
		[SerializeField]                      float _delayInterval    = 0.2f;
		[SerializeField]                      float _resetAngle       = 10f;

		IInputController     _inputController;
		UIController         _uiController;
		Rigidbody            _rigidbody;
		RigidbodyConstraints _constraints;

		bool _interactable = true;
		
		float _delayTime;
		float _flyRotation;
		float _prevRotation;
		float _resetRotation;
		float _deceleration = 1;

		#endregion

		#region public methods

		public override void Init(IInputController inputController, UIController uiController)
		{
			_uiController            =  uiController;
			_inputController         =  inputController;
			_inputController.OnClick += InputController_OnClick;
		}

		#endregion

		#region engine methods

		void Awake()
		{
			_rigidbody   = GetComponent<Rigidbody>();
			_constraints = _rigidbody.constraints;
		}

		void FixedUpdate()
		{
			if (_rigidbody.isKinematic || !_interactable)
				return;

			float rotation = transform.localEulerAngles.z;
			_flyRotation += VectorUtils.GetShortestRotation(rotation, _prevRotation);
			float phase            = rotation / 360f;
			float rotationMultiply = _flyRotation > 360f ? _rotationCurve.Evaluate(phase) * _deceleration : 1f;

			_rigidbody.angularVelocity = new Vector3(0, 0, -_torque) * rotationMultiply;
			_deceleration              = 1f;
			_prevRotation              = rotation;
		}

		void OnDestroy()
		{
			_inputController.OnClick -= InputController_OnClick;
		}

		void OnCollisionEnter(Collision other)
		{
			if (!_interactable || Time.time - _delayTime < _delayInterval)
				return;

			int otherLayer = other.gameObject.layer;
			int thisLayer  = other.GetContact(0).thisCollider.gameObject.layer;

			if (otherLayer == Layers.Ignore)
				return;

			if (otherLayer == Layers.Floor && thisLayer == Layers.Blade)
			{
				Hold();
				return;
			}

			if (thisLayer != Layers.Handle)
				return;

			Back();
		}

		void OnTriggerEnter(Collider other)
		{
			int otherLayer = other.gameObject.layer;
			if (otherLayer != Layers.Respawn)
				return;

			Fall();
		}

		void OnTriggerStay(Collider other)
		{
			int layer = other.gameObject.layer;
			if (layer != Layers.Ignore && layer != Layers.Obstacle)
				return;

			_deceleration = _decelerationRate;
		}

		#endregion

		#region service methods

		void InputController_OnClick()
		{
			Throw();
		}

		void Throw()
		{
			if (_rigidbody.isKinematic)
				_delayTime = Time.time;

			ResetRotation();

			Vector3 force = IsForwardFree() ? _forwardForce : _upForce;

			_rigidbody.AddForce(force, ForceMode.VelocityChange);
		}

		void Back()
		{
			ResetRotation();

			_rigidbody.AddForce(_backwardForce, ForceMode.VelocityChange);
		}

		void Hold()
		{
			_rigidbody.isKinematic = true;
		}

		void Fall()
		{
			_interactable          = false;
			_rigidbody.constraints = RigidbodyConstraints.None;
			_uiController.Fall();
			_inputController.EnableInput(false);
		}

		void ResetRotation()
		{
			_rigidbody.isKinematic     = false;
			_rigidbody.velocity        = Vector3.zero;
			_rigidbody.angularVelocity = Vector3.zero;

			float rotation = transform.localEulerAngles.z;
			_flyRotation  = VectorUtils.GetShortestRotation(rotation, _resetAngle);
			_prevRotation = rotation;
		}

		bool IsForwardFree()
		{
			int layerMask = 1 << Layers.Floor;
			return !Physics.Raycast(transform.position, Vector3.right, FORWARD_DISTANCE, layerMask);
		}

		#endregion
	}
}