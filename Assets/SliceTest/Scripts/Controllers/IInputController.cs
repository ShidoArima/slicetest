﻿using System;

namespace SliceTest.Controllers
{
	public interface IInputController
	{
		event Action OnClick;

		void EnableInput(bool enable);
	}
}