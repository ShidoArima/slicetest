﻿using System;
using UnityEngine;

namespace SliceTest.Controllers
{
	public interface IBonusController
	{
		long               Score  { get; }
		long               Reward { get; }
		event Action<long> OnScoreChange;
		void               Init(Camera                 gameCamera);
		void               ShowBonus(Vector3           position, int bonus);
		void               ShowBonusMultiplier(Vector3 position, int bonus);
		void               Restart();
	}
}