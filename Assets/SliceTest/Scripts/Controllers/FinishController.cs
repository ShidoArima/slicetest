﻿using SliceTest.Environment;
using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Controllers
{
	public class FinishController : MonoBehaviour
	{
		FinishObject[]   _finishObjects;
		IBonusController _bonusController;
		IInputController _inputController;
		UIController     _uiController;

		public void Init(IBonusController bonusController, IInputController inputController, UIController uiController)
		{
			_bonusController = bonusController;
			_inputController = inputController;
			_uiController    = uiController;
		}

		public void Finish(Vector3 position, int bonus)
		{
			_bonusController.ShowBonusMultiplier(position, bonus);
			_inputController.EnableInput(false);
			_uiController.Finish();
		}

		void Awake()
		{
			_finishObjects = GetComponentsInChildren<FinishObject>();

			foreach (FinishObject finishObject in _finishObjects)
			{
				finishObject.Init(this);
			}
		}
	}
}