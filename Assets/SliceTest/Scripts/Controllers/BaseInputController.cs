﻿using System;
using UnityEngine;

namespace SliceTest.Controllers
{
	public abstract class BaseInputController : MonoBehaviour, IInputController
	{
		public abstract event Action OnClick;
		public abstract void         EnableInput(bool enable);
	}
}