﻿using UnityEngine;

namespace SliceTest.Controllers
{
	public class CameraController : MonoBehaviour
	{
		[SerializeField] Transform _target;
		
		Vector3 _prevPosition;
		Vector3 _offset;

		void Awake()
		{
			_offset = transform.position - _target.position;
		}

		void Update()
		{
			Vector3 targetPos = _target.position;
			Vector3 newPos    = Vector3.Lerp(_prevPosition, targetPos, 0.35f);
			_prevPosition = newPos;

			transform.position = new Vector3(newPos.x + _offset.x, newPos.y + _offset.y, transform.position.z);
		}
	}
}