﻿using UnityEngine;

namespace SliceTest.Environment
{
    [RequireComponent(typeof(MeshFilter))]
    public class Sliceable : MonoBehaviour
    {
        public Mesh Mesh { get; private set; }

        protected virtual void Awake()
        {
            Mesh = GetComponent<MeshFilter>().sharedMesh;
        }

    }
}
