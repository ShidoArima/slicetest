﻿using SliceTest.Common;
using SliceTest.Controllers;
using SliceTest.Utils;
using UnityEngine;

namespace SliceTest.Environment
{
	[RequireComponent(typeof(Collider))]
	public class SliceObject : Sliceable
	{
		[SerializeField] Vector3 _sliceForce;
		[SerializeField] int     _bonus = 1;

		IBonusController _bonusController;
		GameObject[]     _slices;
		bool             _sliced;

		public void Init(IBonusController bonusController)
		{
			_bonusController = bonusController;
		}

		protected override void Awake()
		{
			base.Awake();
			Transform t = transform;
		}

		protected void OnTriggerEnter(Collider other)
		{
			if (_sliced)
				return;

			if (other.gameObject.layer != Layers.BladeTrigger)
				return;

			Vector3 dir   = transform.InverseTransformDirection(Vector3.forward);
			Plane   plane = new Plane(dir, Vector3.zero);
			Slice(plane);
		}

		void Slice(Plane slicePlane)
		{
			_sliced = true;
			_slices = Slicer.Slice(slicePlane, this);
			AddForce(_slices[0], true);
			AddForce(_slices[1], false);
			gameObject.SetActive(false);

			_bonusController.ShowBonus(transform.position, _bonus);
		}

		void AddForce(GameObject slice, bool positive)
		{
			slice.layer = Layers.Ignore;
			if (slice.TryGetComponent(out Rigidbody rb))
			{
				Vector3 force = positive ? _sliceForce : -_sliceForce;
				rb.AddForce(force, ForceMode.Impulse);
			}
		}
	}
}