﻿using SliceTest.Common;
using SliceTest.Controllers;
using TMPro;
using UnityEngine;

namespace SliceTest.Environment
{
	[RequireComponent(typeof(Collider))]
	public class FinishObject : MonoBehaviour
	{
		[SerializeField] TMP_Text _bonusText;
		[SerializeField] int      _bonus;
		FinishController          _controller;

		public void Init(FinishController controller)
		{
			_controller = controller;
		}

		void Start()
		{
			if (_bonusText != null)
			{
				_bonusText.text = $"x{_bonus}";
			}
		}

		void OnCollisionEnter(Collision other)
		{
			if(other.collider.gameObject.layer != Layers.Blade)
				return;
			
			_controller.Finish(transform.position, _bonus);
		}
	}
}