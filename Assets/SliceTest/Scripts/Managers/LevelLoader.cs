﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SliceTest.Managers
{
	public class LevelLoader : MonoBehaviour
	{
		static LevelLoader _instance;

		public static event Action<LevelInstaller> OnLevelLoaded;
		
		void Awake()
		{
			_instance = this;
		}

		public static void LoadLevel(int index, Action success = null)
		{
			_instance.LoadLevelInternal(index, success);
		}
		
		void LoadLevelInternal(int index, Action success)
		{
			StartCoroutine(LoadSceneRoutine(index, success));
		}

		IEnumerator LoadSceneRoutine(int index, Action success)
		{
			AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);
			
			while (!asyncLoad.isDone)
			{
				yield return null;
			}
			
			Scene        scene       = SceneManager.GetSceneByBuildIndex(index);
			GameObject[] rootObjects = scene.GetRootGameObjects();

			foreach (GameObject root in rootObjects)
			{
				if (!root.TryGetComponent(out LevelInstaller installer))
					continue;
				
				OnLevelLoaded?.Invoke(installer);
				success?.Invoke();
				yield break;
			}
		}
	}
}