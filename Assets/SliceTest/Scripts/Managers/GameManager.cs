﻿using SliceTest.Controllers;
using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Managers
{
	public class GameManager : MonoBehaviour
	{
		[SerializeField] UIController   _uiController;
		[SerializeField] LevelInstaller _levelInstaller;

		IInputController _inputController;
		IBonusController _bonusController;

		void Start()
		{
			LevelLoader.OnLevelLoaded += LevelLoader_OnLevelLoaded;
			
			_inputController = GetComponentInChildren<IInputController>();
			_bonusController = GetComponentInChildren<IBonusController>();

			_inputController.EnableInput(true);
			_bonusController.Init(Camera.main);
			_uiController.Init(_bonusController);

			if (_levelInstaller == null)
			{
				LevelLoader.LoadLevel(1);
			}
		}

		void LevelLoader_OnLevelLoaded(LevelInstaller installer)
		{
			_bonusController.Restart();
			installer.Init(_inputController, _bonusController, _uiController);
		}
	}
}