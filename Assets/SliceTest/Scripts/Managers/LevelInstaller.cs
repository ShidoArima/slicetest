﻿using SliceTest.Controllers;
using SliceTest.UI;
using UnityEngine;

namespace SliceTest.Managers
{
	public class LevelInstaller : MonoBehaviour
	{
		[SerializeField] BaseKnifeController _knifeController;
		[SerializeField] ObstacleManager     _obstacleManager;
		[SerializeField] FinishController    _finishController;

		public BaseKnifeController KnifeController  => _knifeController;
		public ObstacleManager     ObstacleManager  => _obstacleManager;
		public FinishController    FinishController => _finishController;

		public void Init(IInputController inputController, IBonusController bonusController, UIController uiController)
		{
			_knifeController.Init(inputController, uiController);
			_obstacleManager.Init(bonusController);
			_finishController.Init(bonusController, inputController, uiController);
			
			inputController.EnableInput(true);
		}
	}
}