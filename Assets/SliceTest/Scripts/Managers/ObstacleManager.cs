﻿using SliceTest.Controllers;
using SliceTest.Environment;
using UnityEngine;

namespace SliceTest.Managers
{
	public class ObstacleManager : MonoBehaviour
	{
		IBonusController  _bonusController;
		SliceObject[] _objectList;

		void Awake()
		{
			_objectList = FindObjectsOfType<SliceObject>();
		}

		public void Init(IBonusController bonusController)
		{
			_bonusController = bonusController;

			foreach (SliceObject obstacle in _objectList)
			{
				obstacle.Init(bonusController);
			}
		}
	}
}