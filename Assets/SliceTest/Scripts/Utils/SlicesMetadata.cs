﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SliceTest.Utils
{
	/// <summary>
	/// The side of the mesh
	/// </summary>
	public enum MeshSide
	{
		Positive = 0,
		Negative = 1
	}

	/// <summary>
	/// An object used to manage the positive and negative side mesh data for a sliced object
	/// </summary>
	class SlicesMetadata
	{
		Mesh          _positiveSideMesh;
		List<Vector3> _positiveSideVertices;
		List<int>     _positiveSideTriangles;
		List<Vector2> _positiveSideUvs;
		List<Vector3> _positiveSideNormals;

		Mesh          _negativeSideMesh;
		List<Vector3> _negativeSideVertices;
		List<int>     _negativeSideTriangles;
		List<Vector2> _negativeSideUvs;
		List<Vector3> _negativeSideNormals;

		readonly List<Vector3> _pointsAlongPlane;
		Plane                  _plane;
		Mesh                   _mesh;
		bool                   _useSharedVertices;

		public Mesh PositiveSideMesh
		{
			get
			{
				if (_positiveSideMesh == null)
				{
					_positiveSideMesh = new Mesh();
				}

				SetMeshData(MeshSide.Positive);
				return _positiveSideMesh;
			}
		}

		public Mesh NegativeSideMesh
		{
			get
			{
				if (_negativeSideMesh == null)
				{
					_negativeSideMesh = new Mesh();
				}

				SetMeshData(MeshSide.Negative);

				return _negativeSideMesh;
			}
		}

		public SlicesMetadata(Plane plane, Mesh mesh, bool  shareVertices)
		{
			_positiveSideTriangles = new List<int>();
			_positiveSideVertices  = new List<Vector3>();
			_negativeSideTriangles = new List<int>();
			_negativeSideVertices  = new List<Vector3>();
			_positiveSideUvs       = new List<Vector2>();
			_negativeSideUvs       = new List<Vector2>();
			_positiveSideNormals   = new List<Vector3>();
			_negativeSideNormals   = new List<Vector3>();
			_pointsAlongPlane      = new List<Vector3>();
			_plane                 = plane;
			_mesh                  = mesh;
			_useSharedVertices     = shareVertices;

			ComputeNewMeshes();
		}

		/// <summary>
		/// Add the mesh data to the correct side and calulate normals
		/// </summary>
		void AddTrianglesNormalAndUvs(MeshSide side, Vector3 vertex1, Vector3? normal1, Vector2 uv1, Vector3 vertex2,
		                              Vector3? normal2, Vector2 uv2, Vector3 vertex3, Vector3? normal3, Vector2 uv3,
		                              bool     shareVertices, bool addFirst)
		{
			if (side == MeshSide.Positive)
			{
				AddTrianglesNormalsAndUvs(_positiveSideVertices, _positiveSideTriangles,
				                          _positiveSideNormals, _positiveSideUvs, vertex1, normal1, uv1,
				                          vertex2, normal2, uv2, vertex3, normal3, uv3, shareVertices, addFirst);
			}
			else
			{
				AddTrianglesNormalsAndUvs(_negativeSideVertices, _negativeSideTriangles,
				                          _negativeSideNormals, _negativeSideUvs, vertex1, normal1, uv1,
				                          vertex2, normal2, uv2, vertex3, normal3, uv3, shareVertices, addFirst);
			}
		}


		/// <summary>
		/// Adds the vertices to the mesh sets the triangles in the order that the vertices are provided.
		/// If shared vertices is false vertices will be added to the list even if a matching vertex already exists
		/// Does not compute normals
		/// </summary>
		void AddTrianglesNormalsAndUvs(List<Vector3> vertices, List<int> triangles,
		                                       List<Vector3> normals, List<Vector2> uvs, Vector3 vertex1,
		                                       Vector3?      normal1, Vector2 uv1, Vector3 vertex2, Vector3? normal2,
		                                       Vector2       uv2, Vector3 vertex3, Vector3? normal3, Vector2 uv3,
		                                       bool          shareVertices, bool addFirst)
		{
			int tri1Index = vertices.IndexOf(vertex1);

			if (addFirst)
			{
				ShiftTriangleIndeces(ref triangles);
			}

			//If a the vertex already exists we just add a triangle reference to it, if not add the vert to the list and then add the tri index
			if (tri1Index > -1 && shareVertices)
			{
				triangles.Add(tri1Index);
			}
			else
			{
				if (normal1 == null)
				{
					normal1 = ComputeNormal(vertex1, vertex2, vertex3);
				}

				int? i = null;
				if (addFirst)
				{
					i = 0;
				}

				AddVertNormalUv(ref vertices, ref normals, ref uvs, ref triangles, vertex1, (Vector3) normal1, uv1, i);
			}

			int tri2Index = vertices.IndexOf(vertex2);

			if (tri2Index > -1 && shareVertices)
			{
				triangles.Add(tri2Index);
			}
			else
			{
				if (normal2 == null)
				{
					normal2 = ComputeNormal(vertex2, vertex3, vertex1);
				}

				int? i = null;

				if (addFirst)
				{
					i = 1;
				}

				AddVertNormalUv(ref vertices, ref normals, ref uvs, ref triangles, vertex2, (Vector3) normal2, uv2, i);
			}

			int tri3Index = vertices.IndexOf(vertex3);

			if (tri3Index > -1 && shareVertices)
			{
				triangles.Add(tri3Index);
			}
			else
			{
				if (normal3 == null)
				{
					normal3 = ComputeNormal(vertex3, vertex1, vertex2);
				}

				int? i = null;
				if (addFirst)
				{
					i = 2;
				}

				AddVertNormalUv(ref vertices, ref normals, ref uvs, ref triangles, vertex3, (Vector3) normal3, uv3, i);
			}
		}

		void AddVertNormalUv(ref List<Vector3> vertices,  ref List<Vector3> normals, ref List<Vector2> uvs,
		                     ref List<int>     triangles, Vector3 vertex, Vector3 normal, Vector2 uv, int? index)
		{
			if (index != null)
			{
				int i = (int) index;
				vertices.Insert(i, vertex);
				uvs.Insert(i, uv);
				normals.Insert(i, normal);
				triangles.Insert(i, i);
			}
			else
			{
				vertices.Add(vertex);
				normals.Add(normal);
				uvs.Add(uv);
				triangles.Add(vertices.IndexOf(vertex));
			}
		}

		void ShiftTriangleIndeces(ref List<int> triangles)
		{
			for (int j = 0; j < triangles.Count; j += 3)
			{
				triangles[j]     += +3;
				triangles[j + 1] += 3;
				triangles[j + 2] += 3;
			}
		}

		/// <summary>
		/// Join the points along the plane to the halfway point
		/// </summary>
		void JoinPointsAlongPlane()
		{
			Vector3 halfway = GetHalfwayPoint(out float _);

			for (int i = 0; i < _pointsAlongPlane.Count; i += 2)
			{
				Vector3 firstVertex  = _pointsAlongPlane[i];
				Vector3 secondVertex = _pointsAlongPlane[i + 1];

				Vector3 normal3 = ComputeNormal(halfway, secondVertex, firstVertex);
				normal3.Normalize();

				float direction = Vector3.Dot(normal3, _plane.normal);

				if (direction > 0)
				{
					AddTrianglesNormalAndUvs(MeshSide.Positive, halfway, -normal3, Vector2.zero, firstVertex, -normal3,
					                         Vector2.zero, secondVertex, -normal3, Vector2.zero, false, true);
					AddTrianglesNormalAndUvs(MeshSide.Negative, halfway, normal3, Vector2.zero, secondVertex, normal3,
					                         Vector2.zero,      firstVertex, normal3, Vector2.zero, false, true);
				}
				else
				{
					AddTrianglesNormalAndUvs(MeshSide.Positive, halfway, normal3, Vector2.zero, secondVertex, normal3,
					                         Vector2.zero, firstVertex, normal3, Vector2.zero, false, true);
					AddTrianglesNormalAndUvs(MeshSide.Negative, halfway, -normal3, Vector2.zero, firstVertex, -normal3,
					                         Vector2.zero,      secondVertex, -normal3, Vector2.zero, false, true);
				}
			}
		}

		/// <summary>
		/// For all the points added along the plane cut, get the half way between the first and furthest point
		/// </summary>
		/// <returns></returns>
		Vector3 GetHalfwayPoint(out float distance)
		{
			if (_pointsAlongPlane.Count > 0)
			{
				Vector3 firstPoint    = _pointsAlongPlane[0];
				Vector3 furthestPoint = Vector3.zero;
				distance = 0f;

				foreach (Vector3 point in _pointsAlongPlane)
				{
					float currentDistance = 0f;
					currentDistance = Vector3.Distance(firstPoint, point);

					if (currentDistance > distance)
					{
						distance      = currentDistance;
						furthestPoint = point;
					}
				}

				return Vector3.Lerp(firstPoint, furthestPoint, 0.5f);
			}
			else
			{
				distance = 0;
				return Vector3.zero;
			}
		}

		/// <summary>
		/// Setup the mesh object for the specified side
		/// </summary>
		/// <param name="side"></param>
		void SetMeshData(MeshSide side)
		{
			if (side == MeshSide.Positive)
			{
				_positiveSideMesh.vertices  = _positiveSideVertices.ToArray();
				_positiveSideMesh.triangles = _positiveSideTriangles.ToArray();
				_positiveSideMesh.normals   = _positiveSideNormals.ToArray();
				_positiveSideMesh.uv        = _positiveSideUvs.ToArray();
			}
			else
			{
				_negativeSideMesh.vertices  = _negativeSideVertices.ToArray();
				_negativeSideMesh.triangles = _negativeSideTriangles.ToArray();
				_negativeSideMesh.normals   = _negativeSideNormals.ToArray();
				_negativeSideMesh.uv        = _negativeSideUvs.ToArray();
			}
		}

		/// <summary>
		/// Compute the positive and negative meshes based on the plane and mesh
		/// </summary>
		void ComputeNewMeshes()
		{
			int[]     meshTriangles = _mesh.triangles;
			Vector3[] meshVerts     = _mesh.vertices;
			Vector3[] meshNormals   = _mesh.normals;
			Vector2[] meshUvs       = _mesh.uv;

			for (int i = 0; i < meshTriangles.Length; i += 3)
			{
                //We need the verts in order so that we know which way to wind our new mesh triangles.
                Vector3 vert1      = meshVerts[meshTriangles[i]];
                int     vert1Index = Array.IndexOf(meshVerts, vert1);
                Vector2 uv1        = meshUvs[vert1Index];
                Vector3 normal1    = meshNormals[vert1Index];
                bool    vert1Side  = _plane.GetSide(vert1);

                Vector3 vert2 = meshVerts[meshTriangles[i + 1]];
                int vert2Index = Array.IndexOf(meshVerts, vert2);
                Vector2 uv2 = meshUvs[vert2Index];
                Vector3 normal2 = meshNormals[vert2Index];
                bool vert2Side = _plane.GetSide(vert2);

                Vector3 vert3 = meshVerts[meshTriangles[i + 2]];
                bool vert3Side = _plane.GetSide(vert3);
                int vert3Index = Array.IndexOf(meshVerts, vert3);
                Vector3 normal3 = meshNormals[vert3Index];
                Vector2 uv3 = meshUvs[vert3Index];

				//All verts are on the same side
				if (vert1Side == vert2Side && vert2Side == vert3Side)
				{
					//Add the relevant triangle
					MeshSide side = (vert1Side) ? MeshSide.Positive : MeshSide.Negative;
					AddTrianglesNormalAndUvs(side, vert1, normal1, uv1, vert2, normal2, uv2, vert3, normal3, uv3, true,
					                         false);
				}
				else
				{
					//we need the two points where the plane intersects the triangle.
					Vector3 intersection1;
					Vector3 intersection2;

					Vector2 intersection1Uv;
					Vector2 intersection2Uv;

					MeshSide side1 = (vert1Side) ? MeshSide.Positive : MeshSide.Negative;
					MeshSide side2 = (vert1Side) ? MeshSide.Negative : MeshSide.Positive;

					//vert 1 and 2 are on the same side
					if (vert1Side == vert2Side)
					{
						//Cast a ray from v2 to v3 and from v3 to v1 to get the intersections                       
						intersection1 = GetRayPlaneIntersectionPointAndUv(vert2, uv2, vert3, uv3, out intersection1Uv);
						intersection2 = GetRayPlaneIntersectionPointAndUv(vert3, uv3, vert1, uv1, out intersection2Uv);

						//Add the positive or negative triangles
						AddTrianglesNormalAndUvs(side1, vert1, null, uv1, vert2, null, uv2, intersection1, null,
						                         intersection1Uv, _useSharedVertices, false);
						AddTrianglesNormalAndUvs(side1,         vert1, null, uv1, intersection1, null, intersection1Uv,
						                         intersection2, null,  intersection2Uv, _useSharedVertices, false);

						AddTrianglesNormalAndUvs(side2,         intersection1, null, intersection1Uv, vert3, null, uv3,
						                         intersection2, null, intersection2Uv, _useSharedVertices, false);
					}
					//vert 1 and 3 are on the same side
					else if (vert1Side == vert3Side)
					{
						//Cast a ray from v1 to v2 and from v2 to v3 to get the intersections                       
						intersection1 = GetRayPlaneIntersectionPointAndUv(vert1, uv1, vert2, uv2, out intersection1Uv);
						intersection2 = GetRayPlaneIntersectionPointAndUv(vert2, uv2, vert3, uv3, out intersection2Uv);

						//Add the positive triangles
						AddTrianglesNormalAndUvs(side1, vert1, null, uv1, intersection1, null, intersection1Uv, vert3,
						                         null, uv3, _useSharedVertices, false);
						AddTrianglesNormalAndUvs(side1, intersection1, null, intersection1Uv, intersection2, null,
						                         intersection2Uv, vert3, null, uv3, _useSharedVertices, false);

						AddTrianglesNormalAndUvs(side2,         intersection1, null, intersection1Uv, vert2, null, uv2,
						                         intersection2, null, intersection2Uv, _useSharedVertices, false);
					}
					//Vert1 is alone
					else
					{
						//Cast a ray from v1 to v2 and from v1 to v3 to get the intersections                       
						intersection1 = GetRayPlaneIntersectionPointAndUv(vert1, uv1, vert2, uv2, out intersection1Uv);
						intersection2 = GetRayPlaneIntersectionPointAndUv(vert1, uv1, vert3, uv3, out intersection2Uv);

						AddTrianglesNormalAndUvs(side1,         vert1, null, uv1, intersection1, null, intersection1Uv,
						                         intersection2, null,  intersection2Uv, _useSharedVertices, false);

						AddTrianglesNormalAndUvs(side2, intersection1, null, intersection1Uv, vert2, null, uv2, vert3,
						                         null, uv3, _useSharedVertices, false);
						AddTrianglesNormalAndUvs(side2,         intersection1, null, intersection1Uv, vert3, null, uv3,
						                         intersection2, null, intersection2Uv, _useSharedVertices, false);
					}

					//Add the newly created points on the plane.
					_pointsAlongPlane.Add(intersection1);
					_pointsAlongPlane.Add(intersection2);
				}
			}
			
			JoinPointsAlongPlane();
		}

		Vector3 GetRayPlaneIntersectionPointAndUv(Vector3 vertex1,   Vector2     vertex1Uv, Vector3 vertex2,
		                                          Vector2 vertex2Uv, out Vector2 uv)
		{
			float distance = GetDistanceRelativeToPlane(vertex1, vertex2, out Vector3 pointOfIntersection);
			uv = InterpolateUvs(vertex1Uv, vertex2Uv, distance);
			return pointOfIntersection;
		}

		float GetDistanceRelativeToPlane(Vector3 vertex1, Vector3 vertex2, out Vector3 pointOfintersection)
		{
			Ray ray = new Ray(vertex1, (vertex2 - vertex1));
			_plane.Raycast(ray, out float distance);
			pointOfintersection = ray.GetPoint(distance);
			return distance;
		}

		Vector2 InterpolateUvs(Vector2 uv1, Vector2 uv2, float distance)
		{
			Vector2 uv = Vector2.Lerp(uv1, uv2, distance);
			return uv;
		}

		Vector3 ComputeNormal(Vector3 vertex1, Vector3 vertex2, Vector3 vertex3)
		{
			Vector3 side1 = vertex2 - vertex1;
			Vector3 side2 = vertex3 - vertex1;

			Vector3 normal = Vector3.Cross(side1, side2);

			return normal;
		}
	}
}