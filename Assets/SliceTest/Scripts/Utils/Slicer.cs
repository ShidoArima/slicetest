﻿using SliceTest.Environment;
using UnityEngine;

namespace SliceTest.Utils
{
	//Used https://github.com/Tvtig/UnityLightsaber/
	//Not ideal and have few bugs but works in a term of prototype
	static class Slicer
	{
		public static GameObject[] Slice(Plane plane, Sliceable sliceable)
		{
			Mesh mesh = sliceable.Mesh;

			//Create left and right slice of hollow object
			SlicesMetadata slicesMeta = new SlicesMetadata(plane, mesh, false);

			GameObject positiveObject = CreateMeshGameObject(sliceable);
			positiveObject.name = $"{sliceable.name}_positive";

			GameObject negativeObject = CreateMeshGameObject(sliceable);
			negativeObject.name = $"{sliceable.name}_negative";

			Mesh positiveSideMeshData = slicesMeta.PositiveSideMesh;
			Mesh negativeSideMeshData = slicesMeta.NegativeSideMesh;

			positiveObject.GetComponent<MeshFilter>().mesh = positiveSideMeshData;
			negativeObject.GetComponent<MeshFilter>().mesh = negativeSideMeshData;

			SetupCollidersAndRigidbodies(positiveObject, positiveSideMeshData);
			SetupCollidersAndRigidbodies(negativeObject, negativeSideMeshData);

			return new[] {positiveObject, negativeObject};
		}

		static GameObject CreateMeshGameObject(Sliceable original)
		{
			var originalMaterial = original.GetComponent<MeshRenderer>().materials;

			GameObject meshGameObject = new GameObject();

			meshGameObject.AddComponent<MeshFilter>();
			meshGameObject.AddComponent<MeshRenderer>();

			meshGameObject.GetComponent<MeshRenderer>().materials = originalMaterial;

			Transform originalTransform = original.transform;
			meshGameObject.transform.localScale = originalTransform.localScale;
			meshGameObject.transform.rotation   = originalTransform.rotation;
			meshGameObject.transform.position   = originalTransform.position;
			meshGameObject.transform.SetParent(originalTransform.parent);

			meshGameObject.tag = original.tag;

			return meshGameObject;
		}

		static void SetupCollidersAndRigidbodies(GameObject gameObject, Mesh mesh)
		{
			MeshCollider meshCollider = gameObject.AddComponent<MeshCollider>();
			meshCollider.sharedMesh = mesh;
			meshCollider.convex     = true;

			Rigidbody rb = gameObject.AddComponent<Rigidbody>();
			rb.useGravity = true;
		}
	}
}