﻿using UnityEngine;

namespace SliceTest.Utils
{
	public static class VectorUtils
	{
		public static float GetShortestRotation(float a, float b)
		{
			float diff = Mathf.Abs(a - b);
			return diff < 180f ? diff : 360f - diff;
		}
	}
}